import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

//    Foo f = new Foo; nepouzivat

    static Calculator a;

    @BeforeAll
    public static void initVariable() {

        a = new Calculator();

    }

    @Test
    @Order(1)
    public void add_adds_5And3(){
        a.add(5, 3);
        int expectedResult = 8;

        assertEquals(expectedResult, a.add(5,3));
    }

    @Test
    @Order(2)
    public void substract_substracts_3From5(){
        a.subtract(5, 3);
        int expectedResult = 2;

        assertEquals(expectedResult, a.subtract(5,3));
    }

    @Test
    @Order(3)
    public void multiply_multiplies_2Times5(){
        a.multiply(2, 5);
        int expectedResult = 10;

        assertEquals(expectedResult, a.multiply(2,5));

    }

    @Test
    @Order(4)
    public void divide_divides_8DividedBy2() throws Exception {
        a.divide(8, 2);
        int expectedResult = 4;

        assertEquals(expectedResult, a.divide(8,2));

    }

    @Test
    @Order(5)
    public void divide_throwsException_ExceptionDivisionByZero(){
        Exception exception = Assertions.assertThrows(Exception.class, () -> a.exceptionThrown());

        String expectedMessage = "Division by zero";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);

    }


    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5"})
    public void add_addsAandB_returnsC(int a, int b, int c) {
// arrange
        Calculator calc = new Calculator();
        int expectedResult = c;
// act
        int result = calc.add(a, b);
// assert
        assertEquals(expectedResult, result);
    }

}
