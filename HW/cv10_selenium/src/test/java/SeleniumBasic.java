import dev.failsafe.internal.util.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumBasic {
    WebDriver driver;
    FlightOrder flightOrder;

    @BeforeEach
    public void setUp(){
        driver = new EdgeDriver();
        flightOrder = new FlightOrder(driver);
    }
    @Test
    public void openForm(){
        driver.get("https://ts1.v-sources.eu/");
        String nadpis = driver.findElement(By.tagName("h1")).getAttribute("textContent");
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");
//        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click(); //TODO opravit, nejede
        Assertions.assertEquals("Flight order", nadpis);
//        System.out.println(nadpis);

    }

    @Test
    public void basic_pageObjectModel_test(){
        flightOrder.visitPage();
        Assertions.assertEquals("Flight order", flightOrder.getTitleText());

        flightOrder.setFirstName("ahoj dite me");
        flightOrder.setLastName("jak se mame");
        flightOrder.setEmail("tvoemama@svut.cz");
        flightOrder.setDate("70272003");
        flightOrder.setDestination("Barcelona");
    }

    @Test
    public void sendForm_noName(){
        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click(); //TODO opravit, nejede

    }

}
