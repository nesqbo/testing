package shop;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.internal.matchers.Or;

public class OrderTest {
    //TODO pridat mock testy pro konstruktory, ze byly zavolany; ADD EMPTY ORDERS

    @Test
    public void testConstructorWithState(){
        //act
        StandardItem standardItem = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addItem(standardItem);
        Order order = new Order(shoppingCart, "Karolina Svetla", "Na Balkane 902", 2);

        //assert
        Assertions.assertNotNull(order, "order object should not be null");
        Assertions.assertEquals(shoppingCart.getCartItems(), order.getItems());
//        Assertions.assertEquals(shoppingCart.getTotalPrice(), order.);

        Assertions.assertEquals(shoppingCart.getItemsCount(), order.getItems().size());
        //TODO MUSIM OPRAVIT TUHLE CAST
        Assertions.assertEquals(standardItem, order.getItems().get(0));
        Assertions.assertEquals("Karolina Svetla", order.getCustomerName());
        Assertions.assertEquals("Na Balkane 902", order.getCustomerAddress());
        Assertions.assertEquals(2, order.getState());


        //many items in cart
        StandardItem standardItem1 = new StandardItem(1, "itemOne", 28.9F, "bathroom", 67);
        StandardItem standardItem2 = new StandardItem(6, "itemTwo", 72.9F, "bathroom", 93);
        StandardItem standardItem3 = new StandardItem(21, "itemThree", 62.9F, "bathroom", 0);
        StandardItem standardItem4 = new StandardItem(19, "itemFour", 22.9F, "bathroom", 2);
        StandardItem standardItem5 = new StandardItem(214, "itemFive", 32, "bathroom", 190);

        ShoppingCart shoppingCartManyItems = new ShoppingCart();
        shoppingCartManyItems.addItem(standardItem1);
        shoppingCartManyItems.addItem(standardItem2);
        shoppingCartManyItems.addItem(standardItem3);
        shoppingCartManyItems.addItem(standardItem4);
        shoppingCartManyItems.addItem(standardItem5);

        Order orderManyItems = new Order(shoppingCartManyItems, "hihihehe", "na hradu",89);

        //assert
//        Assertions.assertNotNull(orderManyItems); TODO
        Assertions.assertEquals(shoppingCartManyItems.getCartItems(), orderManyItems.getItems());
        Assertions.assertEquals("hihihehe", orderManyItems.getCustomerName());
        Assertions.assertEquals("na hradu", orderManyItems.getCustomerAddress());
        Assertions.assertEquals(89, orderManyItems.getState());


    }

    @Test
    public void testConstructorNoState() {
        //act
        StandardItem standardItem = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addItem(standardItem);
        Order order = new Order(shoppingCart, "Karolina Svetla", "Na Balkane 902");

        //assert
        Assertions.assertNotNull(order, "order instance should not be null");
        Assertions.assertEquals(shoppingCart.getCartItems(), order.getItems());
//        Assertions.assertEquals(shoppingCart.getTotalPrice(), order.);

        Assertions.assertEquals(shoppingCart.getItemsCount(), order.getItems().size());
        //TODO MUSIM OPRAVIT TUHLE CAST
        Assertions.assertEquals(standardItem, order.getItems().get(0));
        Assertions.assertEquals("Karolina Svetla", order.getCustomerName());
        Assertions.assertEquals("Na Balkane 902", order.getCustomerAddress());


        //many items in cart
        StandardItem standardItem1 = new StandardItem(1, "itemOne", 28.9F, "bathroom", 67);
        StandardItem standardItem2 = new StandardItem(6, "itemTwo", 72.9F, "bathroom", 93);
        StandardItem standardItem3 = new StandardItem(21, "itemThree", 62.9F, "bathroom", 0);
        StandardItem standardItem4 = new StandardItem(19, "itemFour", 22.9F, "bathroom", 2);
        StandardItem standardItem5 = new StandardItem(214, "itemFive", 32, "bathroom", 190);

        ShoppingCart shoppingCartManyItems = new ShoppingCart();
        shoppingCartManyItems.addItem(standardItem1);
        shoppingCartManyItems.addItem(standardItem2);
        shoppingCartManyItems.addItem(standardItem3);
        shoppingCartManyItems.addItem(standardItem4);
        shoppingCartManyItems.addItem(standardItem5);

        Order orderManyItems = new Order(shoppingCartManyItems, "hihihehe", "na hradu");

        //assert
//        Assertions.assertNotNull(orderManyItems); TODO
        Assertions.assertEquals(shoppingCartManyItems.getCartItems(), orderManyItems.getItems());
        Assertions.assertEquals("hihihehe", orderManyItems.getCustomerName());
        Assertions.assertEquals("na hradu", orderManyItems.getCustomerAddress());
//        Assertions.assertEquals();
    }
}
