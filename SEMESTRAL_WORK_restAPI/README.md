<div align="center">
<h1 style="color: red; text-decoration: underline;"><strong>Testování Software</strong></h1>
<h2 style="color: red; text-decoration: underline;">Semestrální práce</h2>
<h2 style="color: red; text-decoration: underline;">Testování REST API - Knihkupectví</h2>
<!-- <h6><a href="https://cataas.com/cat/says/Vasek%20Smitka%20got%20pranked%20haha">Vyzkoušejte zde</a> - work in progress! </h6> -->
<h6>Autoři: 
<a href="https://www.linkedin.com/in/vanessa-le-845624268/">Vanessa Le Nhu Quynh</a>,
<a href="https://www.linkedin.com/in/tobias-le-01b306233/">Tobias Le</a>
</h6>
<h1><a href="https://docs.google.com/document/d/13fS9m87yOrYUBUlWahfqkHb8HmZuijULptf4hyNTCU0/edit?usp=sharing">DOKUMENTACE</a></h1>
<img src="https://i.pinimg.com/564x/7c/6e/9b/7c6e9be750a0aaae6fa05df30bdb62a0.jpg" alt="Bookstore.img">

<table>
    <thead>
        <tr>
            <th><strong>Požadavky</strong></th>
            <th><strong>Popis</strong></th>
            <th><strong>Body</strong></th>
            <th><strong>Splněno</strong></th>
        </tr>
    </thead>
        <tr>
            <td><strong>Návrh testovací strategie</strong></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Popis aplikace</td>
            <td>Popište funkcionalitu aplikace.</td>
            <td>1</td>
            <td>✅</td>
        </tr>
        <tr>
            <td>Testovací strategie</td>
            <td>Vytvořte přehled částí aplikace.</td>
            <td>1</td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Prioritizujte části aplikace.</td>
            <td>1</td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Vypracujte test levels.</td>
            <td>1</td>
            <td>✅</td>
        </tr>
        <tr>
            <td><strong>Testovací scénáře</strong></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Testy vstupů</td>
            <td>Vyberete dva* netriviální vstupy aplikace (např. formulář, rest rozhraní, signatura metody).</td>
            <td></td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Nevolte přihlašovací, registrační a podobné triviální formuláře.</td>
            <td></td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Konzultujte se cvičícím při výberu aplikace.</td>
            <td></td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Pro každý vstupní parametr analyzujte EC a určete mezní podmínky (pokud možno).</td>
            <td>2</td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Vytvořte kombinace testovacích dat technikou pairwise testing.</td>
            <td>2</td>
            <td>✅</td>
        </tr>
        <tr>
            <td>Testy průchodů</td>
            <td>Vytvořte diagramy pro 2* procesy ve vaší aplikaci.</td>
            <td>2</td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Vytvořte procesní testy z těchto diagramů s TDL 2.</td>
            <td>2</td>
            <td>✅</td>
        </tr>
        <tr>
            <td>Detailní testovací scénáře</td>
            <td>Vytvorte 2* detailni testovaci scenare.</td>
            <td>2</td>
            <td>✅</td>
        </tr>
        <tr>
            <td><strong>Implementace testů</strong></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Unit testy</td>
            <td>Vytvořte 10* unit testů testující netriviální metody (netestujte getery, setery a podobně jednoduché metody).</td>
            <td>5</td>
            <td>✅</td>
        </tr>
        <tr>
            <td></td>
            <td>Minimálně 5 (3)* unit testů má používat Mockito.</td>
            <td>5</td>
            <td>✅</td>
        </tr>
        <tr>
            <td>Integrační testy</td>
            <td>Vytvořte 8* integračních/procesních testů.</td>
            <td>6</td>
            <td>✅</td>
        </tr>
    <tfoot>
        <tr>
            <td><strong>CELKEM</strong></td>
            <td></td>
            <td>30</td>
            <td></td>
        </tr>
    </tfoot>
</table>

</div>
