package com.example.bookapi.Author;

import com.example.bookapi.Book.BookException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authors")
public class AuthorController {
    AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public List<Author> getAllAuthors() throws BookException {
        return authorService.getAllAuthors();
    }

    @GetMapping("/id={id}")
    public Author getAuthor(@PathVariable long id) throws AuthorIDException, BookException {
        return authorService.getAuthor(id);
    }

    @PostMapping("/new")
    public void addNewAuthor(@RequestBody Author author) throws AuthorIDException {
        authorService.addNewAuthor(author);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAuthorByID(@PathVariable long id){
        authorService.deleteAuthorById(id);
    }


}
