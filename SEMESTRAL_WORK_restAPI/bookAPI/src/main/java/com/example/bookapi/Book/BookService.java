package com.example.bookapi.Book;
import com.example.bookapi.Author.Author;
import com.example.bookapi.Author.AuthorService;
import com.example.bookapi.Common.Common;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

@Service
public class BookService {

    @Autowired
    private final AuthorService authorService;

    @Autowired
    private final Common common;

    private String JSON_FILE_PATH = "./database/books.json";
    private JSONObject json;


    public BookService(AuthorService authorService, Common common) {
        this.authorService = authorService;
        this.common = common;
        json = common.loadJsonData(JSON_FILE_PATH);
    }

    public void setJSON_FILE_PATH(String JSON_FILE_PATH) {
        this.JSON_FILE_PATH = JSON_FILE_PATH;
        json = common.loadJsonData(this.JSON_FILE_PATH);
    }


    public int getBiggestID() {

        int ID = json.getJSONArray("books").length();
        return ID;
    }

    void setBookJSON(JSONObject json) {
        this.json = json;
    }


    private boolean equalsJSON_Obj(JSONObject object1, JSONObject object2) {
        return (object1.getString("name").trim().equals(object2.getString("name").trim()) &&
                object1.getString("author").trim().equals(object2.getString("author")) &&
                object1.getInt("year") == object2.getInt("year"));
    }

    public List<Book> getAllBooks() {
        JSONArray booksArray = json.getJSONArray("books");
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            if (bookObject.getBoolean("active")) {
                books.add(bookFromObject(bookObject));
            }
        }
        return books;
    }

    public List<Book> getAllBooks(String genre) {
        JSONArray booksArray = json.getJSONArray("books");
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            JSONArray genresArray = bookObject.getJSONArray("genres");
            for (int m = 0; m < genresArray.length(); m++) {
                if (bookObject.getBoolean("active")) {
                    if (genresArray.getString(m).toLowerCase().equals(genre.toLowerCase())) {
                        books.add(bookFromObject(bookObject));
                        break;
                    }
                }
            }
        }
        return books;
    }

    public List<Book> getAllBooks(String author, String genre) {
        JSONArray booksArray = json.getJSONArray("books");
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            JSONArray genresArray = bookObject.getJSONArray("genres");
            if (bookObject.getBoolean("active")) {
                if (bookObject.getString("author").toLowerCase().equals(author.toLowerCase())) {
                    for (int m = 0; m < genresArray.length(); m++) {
                        if (genresArray.getString(m).toLowerCase().equals(genre.toLowerCase())) {
                            books.add(bookFromObject(bookObject));
                            break;
                        }
                    }
                }
            }
        }
        return books;
    }

    public List<Book> getAllBooks(String author, String genre, String publisher) {
        JSONArray booksArray = json.getJSONArray("books");
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            JSONArray genresArray = bookObject.getJSONArray("genres");
            if (bookObject.getString("author").toLowerCase().equals(author.toLowerCase()) &&
                    bookObject.getString("publisher").toLowerCase().equals(publisher.toLowerCase())) {
                if (bookObject.getBoolean("active")) {
                    for (int m = 0; m < genresArray.length(); m++) {
                        if (genresArray.getString(m).toLowerCase().equals(genre.toLowerCase())) {

                            books.add(bookFromObject(bookObject));
                            break;
                        }
                    }
                }
            }
        }
        return books;
    }

    public List<Book> getAllBooks(String author, String genre, int fromPrice, int toPrice) {
        JSONArray booksArray = json.getJSONArray("books");
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            JSONArray genresArray = bookObject.getJSONArray("genres");
            if (bookObject.getString("author").toLowerCase().equals(author.toLowerCase()) &&
                    (bookObject.getInt("price") >= fromPrice) && (bookObject.getInt("price") <= toPrice)) {
                if (bookObject.getBoolean("active")) {
                    for (int m = 0; m < genresArray.length(); m++) {
                        if (genresArray.getString(m).toLowerCase().equals(genre.toLowerCase())) {
                            books.add(bookFromObject(bookObject));
                            break;
                        }
                    }
                }
            }
        }
        return books;
    }

    public List<Book> getAllBooks(String author, String genre, int fromPrice, int toPrice, String publisher) {
        JSONArray booksArray = json.getJSONArray("books");
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            JSONArray genresArray = bookObject.getJSONArray("genres");
            if (bookObject.getString("author").toLowerCase().equals(author.toLowerCase()) && (bookObject.getInt("price") >= fromPrice)
                    && (bookObject.getInt("price") <= toPrice)
                    && bookObject.getString("publisher").toLowerCase().equals(publisher.toLowerCase())) {
                if (bookObject.getBoolean("active")) {
                    for (int m = 0; m < genresArray.length(); m++) {
                        if (genresArray.getString(m).toLowerCase().equals(genre.toLowerCase())) {
                            books.add(bookFromObject(bookObject));
                            break;
                        }
                    }
                }
            }
        }
        return books;
    }

    private Book bookFromObject(JSONObject bookObject) {
        JSONArray genresArray = bookObject.getJSONArray("genres");
        Book book = new Book();
        book.setId(bookObject.getLong("id"));
        book.setName(bookObject.getString("name"));
        book.setAuthor(bookObject.getString("author"));
        book.setCountry(bookObject.getString("country"));
        book.setYear(bookObject.getInt("year"));
        book.setPrice(bookObject.getInt("price"));
        book.setPublisher(bookObject.getString("publisher"));
        book.setActive(bookObject.getBoolean("active"));
        List<String> genresList = new ArrayList<>();
        for (int j = 0; j < genresArray.length(); j++) {
            genresList.add(genresArray.getString(j));
        }
        book.setGenre(genresList);
        book.setStock(bookObject.getInt("stock"));
        return book;
    }

    public Book addBook(Book book) throws BookException {
        //TODO: Check if id is not taken. Check if book isn't already in the database
        book.setId(getBiggestID() + 1);
        JSONObject bookObject = new JSONObject();
        bookObject.put("id", getBiggestID() + 1);
        bookObject.put("name", book.getName());
        bookObject.put("author", book.getAuthor());
        bookObject.put("year", book.getYear());
        bookObject.put("country", book.getCountry());
        bookObject.put("genres", book.getGenre());
        bookObject.put("active", true);
        bookObject.put("stock", book.getStock());
        bookObject.put("publisher", book.getPublisher());
        bookObject.put("price", book.getPrice());

        JSONArray booksArray = json.getJSONArray("books");

        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject eachObject = booksArray.getJSONObject(i);
            if (equalsJSON_Obj(bookObject, eachObject)) {
                throw new BookException("This book is already in the database!");
            }
        }
        booksArray.put(bookObject);
        common.saveJsonData(json, JSON_FILE_PATH);
        authorService.addBookToAuthor(book);

        return bookFromObject(bookObject);
    }


    public Book returnBook(long ID) throws BookException {
        JSONArray booksArray = json.getJSONArray("books");
//    List<Book> books = new ArrayList<>();
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            if (bookObject.getLong("id") == ID) {
                return bookFromObject(bookObject);
            }
        }
        throw new BookException("Book with ID " + ID + " was not found.");
    }

    public void deleteBookByID(long id) throws BookException {
        //TODO:mock database
        JSONArray booksArray = json.getJSONArray("books");
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            if (id == bookObject.getLong("id")) {
                bookObject.put("active", false);
            }
            common.saveJsonData(json, JSON_FILE_PATH);
            return;
        }
        throw new BookException("The book with ID: " + id + " doesn't exist!");
    }

    public void purchaseBook(int id, int amount) throws BookException {
        JSONArray booksArray = json.getJSONArray("books");
        for (int i = 0; i < booksArray.length(); i++) {
            JSONObject bookObject = booksArray.getJSONObject(i);
            if (id == bookObject.getLong("id") && bookObject.getBoolean("active") == true) {
                int stock = bookObject.getInt("stock");
                bookObject.put("stock", stock - amount);
                common.saveJsonData(json, JSON_FILE_PATH);
                return;
            }
        }
        throw new BookException("The book with ID: " + id + " doesn't exist!");

    }

    private void changeAttributesOfBook() {
        Random rand = new Random();
        String[] publishingCompanies = {"Pearson", "RELX Group", "Thomson Reuters", "Penguin Random House",
                "Hachette Livre", "HarperCollins", "Macmillan Publishers", "Wiley", "Scholastic", "Simon & Schuster"};
        JSONArray booksArray = json.getJSONArray("books");
        for (int i = 0; i < booksArray.length(); i++) {
            int price = rand.nextInt(200) + 1;
            int random = rand.nextInt(10);
            String publisher = publishingCompanies[random];
            JSONObject bookObject = booksArray.getJSONObject(i);
            bookObject.put("publisher", publisher);
            bookObject.put("price", price);

        }
        common.saveJsonData(json, JSON_FILE_PATH);
    }

    public int restockBook(int bookID, int amount) throws BookException {
        if (amount > 0) {
            JSONArray booksArray = json.getJSONArray("books");
            for (int i = 0; i < booksArray.length(); i++) {
                JSONObject bookObject = booksArray.getJSONObject(i);
                if (bookID == bookObject.getLong("id")) {
                    int stock = bookObject.getInt("stock");
                    bookObject.put("stock", stock + amount);
                    common.saveJsonData(json, JSON_FILE_PATH);
                    return bookObject.getInt("stock");
                }
            }
            throw new BookException("The book with ID: " + bookID + " doesn't exist!");
        } else {
            throw new BookException("Please enter a valid amount to restock.");
        }
    }
}
