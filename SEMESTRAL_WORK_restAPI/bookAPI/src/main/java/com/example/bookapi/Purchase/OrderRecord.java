package com.example.bookapi.Purchase;

import com.example.bookapi.Common.Common;
import org.json.JSONArray;
import org.json.JSONObject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class OrderRecord {
    private String name;
    private String address;
    private List<Order> orderedBooks;
    private Common common = new Common();


    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public List<Order> getOrderedBooks() {
        return orderedBooks;
    }

    public OrderRecord(String name, String address) {
        this.name = name;
        this.address = address;
        orderedBooks = new ArrayList<Order>();
    }

    public void addOrder(Order order){
        orderedBooks.add(order);
    }

    public void saveRecord(String FILEPATH){
        JSONObject loadedData = common.loadJsonData(FILEPATH);
        JSONArray recordsData = loadedData.getJSONArray("records");

        JSONArray orderArray = new JSONArray();
        for (int i = 0; i < orderedBooks.size(); i++){
            orderArray.put(orderedBooks.get(i).toJson());
        }
        JSONObject recordObject = new JSONObject();
        recordObject.put("name", name);
        recordObject.put("address", address);

        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDateTime = currentDateTime.format(formatter);

        recordObject.put("processed_on", formattedDateTime);
        recordObject.put("order", orderArray);
        recordsData.put(recordObject);
        common.saveJsonData(loadedData, FILEPATH);
    }
}
