package com.example.bookapi.Purchase;

import com.example.bookapi.Book.BookException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/purchase")
public class PurchaseController {

    private PurchaseService purchaseService;

    @Autowired
    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }


    /* BASIC ORDER LOOKS LIKE THIS
    {
      "name": "John Doe",
      "address": "123 Main St.",
      "order": [
        {
          "id": 1,
          "amount": 2
        },
        {
          "id": 3,
          "amount": 1
        },
        {
          "id": 5,
          "amount": 3
        }
      ]
    }
     */
    @PostMapping
    public JSONObject purchaseBooks(@RequestBody JSONObject order) throws BookException, StockException {
        return purchaseService.purchaseBooks(order);
    }
}
