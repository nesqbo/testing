package com.example.bookapi.Book;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    private BookService bookService;



    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public List<Book> getBooks(@RequestParam(required = false) String genre, @RequestParam(required = false) String author,
                               @RequestParam(required = false) String priceRange,
                               @RequestParam(required = false) String publisher) {
        if (author != null && genre != null && priceRange!=null && publisher != null){
            if (priceRange.equals("cheap")){
                return bookService.getAllBooks(author, genre, 0, 50, publisher);
            } else if (priceRange.equals("medium")){
                return bookService.getAllBooks(author, genre, 50, 100, publisher);
            } else if (priceRange.equals("expensive")){
                return bookService.getAllBooks(author, genre, 100, 1000, publisher);
            }
            return bookService.getAllBooks(author, genre, publisher);
        }
        else if (author != null && genre != null && publisher != null){
            return bookService.getAllBooks(author, genre, publisher);
        }
        else if (author != null && genre != null && priceRange!=null){
            if (priceRange.equals("cheap")){
                return bookService.getAllBooks(author, genre, 0, 50);
            } else if (priceRange.equals("medium")){
                return bookService.getAllBooks(author, genre, 50, 100);
            } else if (priceRange.equals("expensive")){
                return bookService.getAllBooks(author, genre, 100, 1000);
            }
            return bookService.getAllBooks(author, genre);
        }
        else if (author != null && genre != null){
            return bookService.getAllBooks(author, genre);
        }
        else if (genre != null) {
            return bookService.getAllBooks(genre);
        }
        else {
            return bookService.getAllBooks();
        }
    }

    @PostMapping("/restock/ID={bookID}/amount={amount}")
    public int restockBook(@PathVariable int bookID, @PathVariable int amount) throws BookException {
        if (amount <= 0){
            throw new BookException("PLEASE ENTER A VALID AMOUNT");
        } else {
            return bookService.restockBook(bookID, amount);
        }
    }

    @PostMapping
    public ResponseEntity<Book> addBook(@RequestBody Book newBook) throws BookException {
        Book book = bookService.addBook(newBook);
        return new ResponseEntity<>(book, HttpStatus.CREATED);
    }

    @GetMapping("/ID={ID}")
    public Book returnBook(@PathVariable long ID) throws BookException {
        return bookService.returnBook(ID);
    }

    @DeleteMapping("/delete/ID={ID}")
    public void deleteBookByID(@PathVariable long ID) throws BookException {
        bookService.deleteBookByID(ID);
    }

//    @DeleteMapping(/delete)
//    public void deleteBookByJSON(@RequestBody Book book) throws BookException{
//        bookService.deleteBookByJSON(book);
//    }


}
