package com.example.bookapi.Author;

import com.example.bookapi.Book.Book;
import com.example.bookapi.Book.BookException;
import com.example.bookapi.Book.BookService;
import com.example.bookapi.Common.Common;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService {

    @Autowired
    private final Common common;

    private static String JSON_FILE_PATH = "./database/authors.json";
    private final BookService bookService;
    private JSONObject json;

    public AuthorService(Common common) {
        this.common = common;
        bookService = new BookService(this, common);
        json = common.loadJsonData(JSON_FILE_PATH);
    }

    public void setAuthorJSON(JSONObject json) {

        this.json = json;

    }

    public List<Author> getAllAuthors() throws BookException {
        JSONArray authorsArray = json.getJSONArray("authors");

        List<Author> authors = new ArrayList<>();
        for (int i = 0; i < authorsArray.length(); i++) {
            JSONObject authorObject = authorsArray.getJSONObject(i);
            Author author = fillAuthor(authorObject);
            authors.add(author);
        }
        return authors;
    }

    public Author getAuthor(long id) throws AuthorIDException, BookException {
        JSONArray authorsArray = json.getJSONArray("authors");

        for (int i = 0; i < authorsArray.length(); i++) {
            JSONObject authorObject = authorsArray.getJSONObject(i);
            if (authorObject.getLong("author_id") == id){
                Author author = fillAuthor(authorObject);
                return author;
            }
        }
        throw new AuthorIDException("Author with id " + id + " does not exist");
    }

    private Author fillAuthor(JSONObject authorObject) throws BookException {
        Author author = new Author();
        author.setAuthorId(authorObject.getInt("author_id"));
        author.setAuthorName(authorObject.getString("author_name"));
        author.setCountryOfOrigin(authorObject.getString("country_of_origin"));
        JSONArray booksArray = authorObject.getJSONArray("books");
        List<Integer> books = new ArrayList<>();
        List<Book> booksList = new ArrayList<>();
        for (int j = 0; j < booksArray.length(); j++) {
            int bookId = booksArray.getInt(j);
            books.add(bookId);
            booksList.add(bookService.returnBook(bookId));
        }
        if (authorObject.getBoolean("active")) {
            author.setActive();
        } else {
            author.setInactive();
        }
        author.setBooks(books);
        author.setBooksList(booksList);
        return author;
    }

    public void addNewAuthor(Author author) throws AuthorIDException {
        JSONArray authorsArray = json.getJSONArray("authors");

        for (int i = 0; i < authorsArray.length(); i++) {
            JSONObject authorObject = authorsArray.getJSONObject(i);
            if (authorObject.getLong("author_id") == author.getAuthorId()){
                throw new AuthorIDException("Author with id " + author.getAuthorId() + " already exists");
            }
            if (authorObject.getString("author_name") == author.getAuthorName() &&
            authorObject.getString("country_of_origin") == author.getCountryOfOrigin()){
                throw new AuthorIDException("Author " + author.getAuthorName() + " from " +
                        author.getCountryOfOrigin() + " already exists");
            }
        }

        JSONObject newAuthorObject = new JSONObject();
        newAuthorObject.put("author_id", author.getAuthorId());
        newAuthorObject.put("author_name", author.getAuthorName());
        newAuthorObject.put("country_of_origin", author.getCountryOfOrigin());
        newAuthorObject.put("books", new JSONArray(author.getBooks()));
        authorsArray.put(newAuthorObject);
        common.saveJsonData(json, JSON_FILE_PATH);

    }

    public void deleteAuthorById(long id) {
        JSONArray authorsArray = json.getJSONArray("authors");

        for (int i = 0; i < authorsArray.length(); i++) {
            JSONObject authorObject = authorsArray.getJSONObject(i);
            if (authorObject.getLong("author_id") == id){
                authorObject.put("active", false);
            }
        common.saveJsonData(json, JSON_FILE_PATH);
        }
    }

    private int getHighestId(){
        JSONArray authorsArray = json.getJSONArray("authors");
        int highest_id = -1;
        for (int i = 0; i < authorsArray.length(); i++) {
            JSONObject authorObject = authorsArray.getJSONObject(i);
            System.out.println(authorObject);
            if (authorObject.getInt("author_id") > highest_id) {
                highest_id = authorObject.getInt("author_id");
            }
        }
        return highest_id;
    }

    public void addBookToAuthor(Book book){
        JSONArray authorsArray = json.getJSONArray("authors");
        for (int i = 0; i < authorsArray.length(); i++) {
            JSONObject authorObject = authorsArray.getJSONObject(i);
            System.out.println(authorObject.getString("author_name").trim() + " " + book.getAuthor().trim());
            System.out.println(authorObject.getString("author_name").trim().equals(book.getAuthor().trim()));
            if (authorObject.getString("author_name").trim().equals(book.getAuthor().trim())) {
                JSONArray bookArray = authorObject.getJSONArray("books");
                bookArray.put(book.getId());
                common.saveJsonData(json, JSON_FILE_PATH);
                System.out.println(bookArray);
                return;
            }
        }
        List<Integer> tempList = new ArrayList<>();
        tempList.add((int) book.getId());
        createNewAuthor(book.getAuthor(), book.getCountry(), tempList);
    }

    public void createNewAuthor(String author, String country, List<Integer> books) {
        JSONArray authorsArray = json.getJSONArray("authors");
        Author newAuthor = new Author(getHighestId()+1, author, country, books);
        JSONObject authorObject = new JSONObject();
        authorObject.put("author_id", newAuthor.getAuthorId());
        authorObject.put("author_name", newAuthor.getAuthorName());
        authorObject.put("country_of_origin", newAuthor.getCountryOfOrigin());
        authorObject.put("books", newAuthor.getBooks());
        authorObject.put("active", true);
        authorsArray.put(authorObject);
        common.saveJsonData(json, JSON_FILE_PATH);
    }

    public void setJSON_FILE_PATH(String path) {
        JSON_FILE_PATH = path;
    }
}
