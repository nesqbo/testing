package com.example.bookapi.Purchase;

import com.example.bookapi.Author.AuthorService;
import com.example.bookapi.Book.BookException;
import com.example.bookapi.Book.BookService;
import com.example.bookapi.Common.Common;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class PurchaseService {

    BookService bookService;

    private String JSON_FILE_PATH = "./database/orders.json";

    public void setJSON_FILE_PATH(String path){
        this.JSON_FILE_PATH = path;
    }

    public PurchaseService(BookService bookService) {
        this.bookService = bookService;
    }

    public JSONObject purchaseBooks(JSONObject order) throws BookException, StockException {
        JSONArray ordersArray = order.getJSONArray("order");
        JSONArray toProcess = new JSONArray();
        for (int i = 0; i < ordersArray.length(); i++){
            JSONObject bookOrder = ordersArray.getJSONObject(i);
            if (bookOrder.getInt("id") <= bookService.getBiggestID() || (bookOrder.getBoolean("active") == true)){
                if (!bookOrder.has("amount") || bookOrder.getInt("amount") == 0){
                    throw new StockException("Invalid amount.");
                }
                else if (bookOrder.getInt("amount") <= bookService.returnBook(bookOrder.getInt("id")).getStock()){
                    toProcess.put(bookOrder);
                }
                else {
                    throw new StockException(bookService.returnBook(bookOrder.getInt("id")).getName() +" (id = "
                            + bookOrder.getInt("id") + ") only has "
                            + bookService.returnBook(bookOrder.getInt("id")).getStock() + " copies and you ordered " +
                            bookOrder.getInt("amount"));
                }
            } else {
                throw new BookException("Book with id = " +bookOrder.getInt("id") + " does not exist" );
            }
        }
        return processOrders(order, toProcess);
    }

    private JSONObject processOrders(JSONObject order, JSONArray toProcess) throws BookException {
        JSONObject processed = new JSONObject();
        JSONArray processedArray = new JSONArray();
        OrderRecord orderRecord = new OrderRecord(order.getString("name"), order.getString("address"));
        for (int i = 0; i < toProcess.length(); i++){
            Order orderedBook = new Order(bookService.returnBook(toProcess.getJSONObject(i).getInt("id")),
                    toProcess.getJSONObject(i).getInt("amount"));
            orderRecord.addOrder(orderedBook);
            processedArray.put(toProcess.getJSONObject(i));
            bookService.purchaseBook(toProcess.getJSONObject(i).getInt("id"),
                    toProcess.getJSONObject(i).getInt("amount"));
        }
        processed.put("name", order.getString("name"));
        processed.put("address", order.getString("address"));
        processed.put("processed", processedArray);
        orderRecord.saveRecord(JSON_FILE_PATH);
        return processed;
    }

    //FOR QUICK TESTING IGNORE
//    public static void main(String[] args) throws BookException, StockException {
//        PurchaseService purchaseService = new PurchaseService(new BookService(new AuthorService(new Common()), new Common()));
//        String order = "{\n" +
//                "      \"name\": \"Vanessa Blba\",\n" +
//                "      \"address\": \"421111 Main St.\",\n" +
//                "      \"order\": [\n" +
//                "        {\n" +
//                "          \"id\": 16,\n" +
//                "          \"amount\": 2\n" +
//                "        },\n" +
//                "        {\n" +
//                "          \"id\": 4,\n" +
//                "          \"amount\": 2\n" +
//                "        },\n" +
//                "        {\n" +
//                "          \"id\": 30,\n" +
//                "          \"amount\": 1\n" +
//                "        }\n" +
//                "      ]\n" +
//                "    }";
//        purchaseService.purchaseBooks(new JSONObject(order));
//    }
}
