package com.example.bookapi.Author;

public class AuthorIDException extends Exception {

    // Constructors
    public AuthorIDException() {
        super();
    }

    public AuthorIDException(String message) {
        super(message);
    }

    public AuthorIDException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorIDException(Throwable cause) {
        super(cause);
    }
}
