package com.example.bookapi.Common;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class Common {
    public JSONObject loadJsonData(String JSON_FILE_PATH) {
        try {
            FileReader reader = new FileReader(JSON_FILE_PATH);
            JSONTokener tokener = new JSONTokener(reader);
            return new JSONObject(tokener);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveJsonData(JSONObject json, String JSON_FILE_PATH) {
        if (JSON_FILE_PATH== null){
            return;
        }
        try (FileWriter fileWriter = new FileWriter(JSON_FILE_PATH)) {
            fileWriter.write(json.toString(4)); // 4 is the number of spaces for indentation
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
