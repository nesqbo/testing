package com.example.bookapi.Book;

import org.json.JSONObject;

import java.util.List;

public class Book {
    private long id;
    private String name;
    private String author;
    private String country;
    private int year;
    private List<String> genre;
    private int stock;
    private String publisher;
    private int price;

    private boolean active = false;
    // Constructor, getters, setters

    public Book(long id, String name, String author, String country, int year, List<String> genre, int stock,
                String publisher, int price) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.country = country;
        this.year = year;
        this.genre = genre;
        this.price = price;
        this.publisher = publisher;
        this.stock = stock;
    }

    public Book() {
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getCountry() {
        return country;
    }

    public int getYear() {
        return year;
    }

    public List<String> getGenre() {
        return genre;
    }

    public boolean getActive(){
        return active;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return year == book.year && name.trim().equals(book.name.trim()) &&
                author.trim().equals(book.author.trim());
    }
}
