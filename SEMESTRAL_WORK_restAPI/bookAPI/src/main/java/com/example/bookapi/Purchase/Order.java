package com.example.bookapi.Purchase;

import com.example.bookapi.Book.Book;
import org.json.JSONObject;

public class Order {
    private Book book;
    private int amount;

    public Order(Book book, int amount) {
        this.book = book;
        this.amount = amount;
    }

    public JSONObject toJson(){
        JSONObject orderObject = new JSONObject();
        orderObject.put("book_name", book.getName());
        orderObject.put("book_author", book.getAuthor());
        orderObject.put("id", book.getId());
        orderObject.put("amount", amount);
        return orderObject;
    }
}
