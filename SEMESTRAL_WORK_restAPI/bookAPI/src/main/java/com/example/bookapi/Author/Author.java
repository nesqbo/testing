package com.example.bookapi.Author;

import com.example.bookapi.Book.Book;

import java.util.List;

import java.util.List;
import java.util.Objects;

public class Author {
    private int authorId;
    private String authorName;
    private String countryOfOrigin;
    private List<Integer> books;

    private boolean active = false;

    private List<Book> booksList;

    public Author(int authorId, String authorName, String countryOfOrigin, List<Integer> books) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.countryOfOrigin = countryOfOrigin;
        this.books = books;
        this.active = true;
    }

    public Author() {
        this.active = true;
    }

    // Getters and Setters
    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    public List<Integer> getBooks() {
        return books;
    }

    public void setBooks(List<Integer> books) {
        this.books = books;
    }

    public boolean isActive() {
        return active;
    }

    public void setInactive(){
        this.active = false;
    }

    public void setActive(){
        this.active = true;
    }

    public List<Book> getBooksList() {
        return booksList;
    }

    public void setBooksList(List<Book> booksList) {
        this.booksList = booksList;
    }

    // toString method
    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", countryOfOrigin='" + countryOfOrigin + '\'' +
                ", books=" + books +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(authorName, author.authorName) && Objects.equals(countryOfOrigin, author.countryOfOrigin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorName, countryOfOrigin);
    }
}

