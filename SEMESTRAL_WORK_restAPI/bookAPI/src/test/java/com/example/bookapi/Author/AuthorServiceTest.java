package com.example.bookapi.Author;

import com.example.bookapi.Book.Book;
import com.example.bookapi.Book.BookException;
import com.example.bookapi.Book.BookService;
import com.example.bookapi.Common.Common;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AuthorServiceTest {

    @InjectMocks
    private AuthorService authorService;

    @Mock
    private Common common;

    @Mock
    private BookService bookService;

    private JSONObject json;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        json = new JSONObject();
        JSONArray authorsArray = new JSONArray();
        json.put("authors", authorsArray);
        authorService.setAuthorJSON(json);
    }

    @Test
    public void testGetAllAuthors() throws BookException {
        //ARRANGE
        when(common.loadJsonData(anyString())).thenReturn(json);

        JSONArray authorsArray = json.getJSONArray("authors");
        JSONObject author1 = new JSONObject();
        author1.put("author_id", 1);
        author1.put("author_name", "Author1");
        author1.put("country_of_origin", "Country1");
        author1.put("books", new JSONArray());
        author1.put("active", true);
        authorsArray.put(author1);
        JSONObject author2 = new JSONObject();
        author2.put("author_id", 2);
        author2.put("author_name", "Author2");
        author2.put("country_of_origin", "Country2");
        author2.put("books", new JSONArray());
        author2.put("active", true);
        authorsArray.put(author2);

        //ACT
        List<Author> authors = authorService.getAllAuthors();

        //ASSERT
        verify(common, times(2)).loadJsonData(anyString());
        verifyNoMoreInteractions(common);
        verifyNoInteractions(bookService);
        assertNotNull(authors);
        assertEquals(2, authors.size());
        assertEquals("Author1", authors.get(0).getAuthorName());
        assertEquals("Country1", authors.get(0).getCountryOfOrigin());
        assertEquals("Author2", authors.get(1).getAuthorName());
        assertEquals("Country2", authors.get(1).getCountryOfOrigin());
    }

    @Test
    public void testGetAuthorWithValidId() throws AuthorIDException, BookException {
        //ARRANGE
        JSONArray authorsArray = new JSONArray();
        JSONObject authorObject = new JSONObject();
        authorObject.put("author_id", 1);
        authorObject.put("author_name", "John Doe");
        authorObject.put("country_of_origin", "United States");
        authorObject.put("books", new JSONArray());
        authorObject.put("active", true);
        authorsArray.put(authorObject);
        json.put("authors", authorsArray);
        authorService.setAuthorJSON(json);
        long id = 1;

        //ACT
        Author author = authorService.getAuthor(id);

        //ASSERT
        assertNotNull(author);
        assertEquals(1, author.getAuthorId());
        assertEquals("John Doe", author.getAuthorName());
        assertEquals("United States", author.getCountryOfOrigin());
        assertTrue(author.isActive());
        assertEquals(0, author.getBooks().size());
        assertEquals(0, author.getBooksList().size());
    }
}