package com.example.bookapi.Book;

import com.example.bookapi.Author.AuthorService;
import com.example.bookapi.Common.Common;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SpringBootTest
public class BookServiceTest {
    @Mock
    private AuthorService authorService;

    @Mock
    private Common common;

    @InjectMocks
    private BookService bookService;

    private JSONObject json;

    @BeforeEach
    public void setUp() {
        bookService.setJSON_FILE_PATH(null);
        json = new JSONObject();
        JSONArray booksArray = new JSONArray();
        json.put("books", booksArray);
        bookService.setBookJSON(json);
    }

    @Test
    public void testGetAllBooks_returnsArray_emptyArray() {
        // TODO: Add test case for getAllBooks() method
        //ARRANGE
        List<Book> expectedResult = new ArrayList<>();

        //ACT
        List<Book> result = bookService.getAllBooks();

        //ASSERT
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void testGetAllBooks_returnsArray_arrayWithBook1() throws BookException {
        // TODO: Add test case for getAllBooks() method
        //ARRANGE
        Book book = mock(Book.class);
        when(book.getName()).thenReturn("Test Book");
        when(book.getAuthor()).thenReturn("Test Author");
        when(book.getYear()).thenReturn(2023);
        when(book.getCountry()).thenReturn("Test Country");
        when(book.getPublisher()).thenReturn("Test Publisher");
        when(book.getGenre()).thenReturn(null);

        //ACT
        bookService.addBook(book);
        List<Book> result = bookService.getAllBooks();

        //ASSERT
        Assertions.assertEquals(book.getName(), result.get(0).getName());
        Assertions.assertEquals(book.getYear(), result.get(0).getYear());
        Assertions.assertEquals(book.getAuthor(), result.get(0).getAuthor());
        Assertions.assertEquals(book.getCountry(), result.get(0).getCountry());
    }

    @Test
    public void testAddBook_addsBook_book1() throws BookException {
        // TODO: Add test case for addBook() method
        //ARRANGE
        Book book = mock(Book.class);
        when(book.getName()).thenReturn("Test Book");
        when(book.getAuthor()).thenReturn("Test Author");
        when(book.getYear()).thenReturn(2023);
        when(book.getCountry()).thenReturn("Test Country");
        when(book.getGenre()).thenReturn(null);
        when(book.getPublisher()).thenReturn("Test Publisher");
        when(book.getPrice()).thenReturn(1);

        //ACT
        bookService.addBook(book);
        List<Book> result = bookService.getAllBooks();

        //ASSERT
        verify(common, times(1)).saveJsonData(any(), any());
        verify(authorService, times(1)).addBookToAuthor(any());
        Assertions.assertEquals(book.getName(), result.get(0).getName());
        Assertions.assertEquals(book.getYear(), result.get(0).getYear());
        Assertions.assertEquals(book.getAuthor(), result.get(0).getAuthor());
        Assertions.assertEquals(book.getCountry(), result.get(0).getCountry());
        Assertions.assertEquals(book.getPublisher(), result.get(0).getPublisher());
        Assertions.assertEquals(book.getPrice(), result.get(0).getPrice());
    }

    @Test
    public void testAddBook_addsDuplicateBook_throwsBookException() throws BookException {
        // TODO: Add test case for addBook() method
        //ARRANGE
        Book book = mock(Book.class);
        when(book.getName()).thenReturn("Test Book");
        when(book.getAuthor()).thenReturn("Test Author");
        when(book.getYear()).thenReturn(2023);
        when(book.getCountry()).thenReturn("Test Country");
        when(book.getGenre()).thenReturn(null);
        when(book.getPublisher()).thenReturn("Test Publisher");
        String expectedMessage = "This book is already in the database!";

        //ACT
        bookService.addBook(book);

        //ASSERT
        BookException ex = Assertions.assertThrows(BookException.class, () -> {
            bookService.addBook(book);
        });
        Assertions.assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    public void testReturnBook_returnsBook_book1() throws BookException {
        // TODO: Add test case for returnBook() method
        //ARRANGE
        Book book = mock(Book.class);
        when(book.getName()).thenReturn("Test Book");
        when(book.getAuthor()).thenReturn("Test Author");
        when(book.getYear()).thenReturn(2023);
        when(book.getCountry()).thenReturn("Test Country");
        when(book.getGenre()).thenReturn(null);
        when(book.getPublisher()).thenReturn("Test Publisher");

        //ACT
        bookService.addBook(book);
        Book result = bookService.returnBook(1);

        //ASSERT
        verify(common, times(1)).saveJsonData(any(), any());
        verify(authorService, times(1)).addBookToAuthor(any());
        Assertions.assertEquals(book.getName(), result.getName());
        Assertions.assertEquals(book.getYear(), result.getYear());
        Assertions.assertEquals(book.getAuthor(), result.getAuthor());
        Assertions.assertEquals(book.getCountry(), result.getCountry());
    }

    @Test
    public void testReturnBook_throwsException_BookException() throws BookException {
        // TODO: Add test case for returnBook() method
        //ARRANGE
        String expectedMessage = "Book with ID 15 was not found.";

        //ACT+ASSERT
        BookException ex = Assertions.assertThrows(BookException.class, () -> {
            bookService.returnBook(15);
        });
        Assertions.assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    public void testDeleteBookByID_changesActive_false() throws BookException {
        //ARRANGE
        Book book = mock(Book.class);
        when(book.getName()).thenReturn("Test Book");
        when(book.getAuthor()).thenReturn("Test Author");
        when(book.getYear()).thenReturn(2023);
        when(book.getCountry()).thenReturn("Test Country");
        when(book.getPublisher()).thenReturn("Test Publisher");
        when(book.getGenre()).thenReturn(null);

        //ACT
        bookService.addBook(book);
        bookService.deleteBookByID(1);
        List<Book> result = bookService.getAllBooks();

        //ASSERT
        Assertions.assertEquals(new ArrayList<Book>()    ,result);
    }

    @Test
    public void testDeleteBookByID_throwsException_BookException() throws BookException {
        // TODO: Add test case for deleteBookByID() method
        //ARRANGE
        String expectedMessage = "The book with ID: 24 doesn't exist!";

        //ACT+ASSERT
        BookException ex = Assertions.assertThrows(BookException.class, () ->{
            bookService.deleteBookByID(24);
        });
        Assertions.assertEquals(expectedMessage, ex.getMessage());
    }


    //PROCESNI TESTY

    @Test
    public void procesniTest1_standartniRestock() throws BookException {
        //ARRANGE
        common = new Common();
        authorService = new AuthorService(common);
        bookService = new BookService(authorService, common);
        bookService.setJSON_FILE_PATH("./src/test/java/testDB/books.json");
        String authorName = "Stieg Larsson";
        String genreName = "Thriller";
        String bookName = "The Girl with the Dragon Tattoo";
        String publisherName = "Simon & Schuster";
        int bookID = 15;
        String authorOrigin = "Sweden";
        int publishedInYear = 2005;
        boolean activityStatus = true;
        int price = 97;

        //ACT
        //KROK 1 - VYHLEDAT KNIHU PODLE AUTORA A ZANRU
        List<Book> foundBooks = bookService.getAllBooks(authorName, genreName);
        Book book = foundBooks.get(0);

        //ASSERT
        Assertions.assertEquals(bookName, book.getName());
        Assertions.assertEquals(publishedInYear, book.getYear());
        Assertions.assertEquals(authorName, book.getAuthor());
        Assertions.assertEquals(authorOrigin, book.getCountry());
        Assertions.assertEquals(publisherName, book.getPublisher());
        Assertions.assertEquals(bookID, book.getId());
        Assertions.assertEquals(activityStatus, book.getActive());
        Assertions.assertEquals(price, book.getPrice());

        //KROK 2 - ULOZIT SI STOCK PRED
        int stockBeforeRestock = book.getStock();

        //KROK 3 - RESTOCK
        bookService.restockBook(bookID, 15);
        book = bookService.returnBook(bookID);

        //KROK 4 - KONTROLA PO RESTOCKU - ASSERT
        Assertions.assertEquals(bookName, book.getName());
        Assertions.assertEquals(publishedInYear, book.getYear());
        Assertions.assertEquals(authorName, book.getAuthor());
        Assertions.assertEquals(authorOrigin, book.getCountry());
        Assertions.assertEquals(publisherName, book.getPublisher());
        Assertions.assertEquals(bookID, book.getId());
        Assertions.assertEquals(activityStatus, book.getActive());
        Assertions.assertEquals(price, book.getPrice());
        Assertions.assertEquals(stockBeforeRestock+15, book.getStock());
    }

    @Test
    public void procesniTest2_restockNeexistujiciKnihy() throws BookException {
        //ARRANGE
        common = new Common();
        authorService = new AuthorService(common);
        bookService = new BookService(authorService, common);
        bookService.setJSON_FILE_PATH("./src/test/java/testDB/books.json");

        int idOfNonexistantBook = bookService.getBiggestID()+1;
        String expectedMessage = "The book with ID: "+idOfNonexistantBook+" doesn't exist!";

        //ACT+ASSERT
        BookException ex = Assertions.assertThrows(BookException.class, () -> {
            bookService.restockBook(idOfNonexistantBook, 420);
        });
        Assertions.assertEquals(expectedMessage, ex.getMessage());
    }

    @Test
    public void procesniTest3_restockZapornehoPoctu() throws BookException {
        //ARRANGE
        common = new Common();
        authorService = new AuthorService(common);
        bookService = new BookService(authorService, common);
        bookService.setJSON_FILE_PATH("./src/test/java/testDB/books.json");
        int bookID = 15;
        String expectedMessage = "Please enter a valid amount to restock.";


        //ACT
        Book book = bookService.returnBook(bookID);
        int stockBeforeRestock = book.getStock();

        //ACT+ASSERT
        BookException ex = Assertions.assertThrows(BookException.class, () -> {
            bookService.restockBook(bookID, -5);
        });
        Assertions.assertEquals(expectedMessage, ex.getMessage());

        //ASSERT
        book = bookService.returnBook(bookID);
        int stockAfterRestock = book.getStock();
        Assertions.assertEquals(stockBeforeRestock, stockAfterRestock);
    }
}
