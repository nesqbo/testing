package com.example.bookapi.Book;

import com.example.bookapi.Author.AuthorService;
import com.example.bookapi.Common.Common;
import com.example.bookapi.Purchase.PurchaseService;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BookControllerTest {
    BookService bookService;
    Common common;
    AuthorService authorService;
    JSONObject book = new JSONObject();

    @BeforeEach
    public void setUp(){
        common = new Common();
        authorService = new AuthorService(common);
        bookService = new BookService(authorService, common);
    }

}
