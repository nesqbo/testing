package com.example.bookapi.Purchase;

import com.example.bookapi.Author.AuthorService;
import com.example.bookapi.Book.Book;
import com.example.bookapi.Book.BookException;
import com.example.bookapi.Book.BookService;
import com.example.bookapi.Common.Common;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PurchaseControllerTest {
    PurchaseService purchaseService;
    BookService bookService;
    Common common;
    AuthorService authorService;

    Order order;
    OrderRecord orderRecord;

    String address;
    String name;
    String date;
    String orderName;
    String orderAddress;
    int bookAmount;
    String bookAuthor;
    int bookId;
    String bookName;
//    JSONObject book = new JSONObject();
    String JSON_FILE_PATH;
    int receiptAmountBefore, receiptAmountAfter;
    @BeforeEach
    public void setUp(){
        common = new Common();
        authorService = new AuthorService(common);
        bookService = new BookService(authorService, common);
        purchaseService = new PurchaseService(bookService);
//        order = new Order();
//        orderRecord = new OrderRecord();
        JSON_FILE_PATH = "./src/test/java/testDB/orders.json";
        purchaseService.setJSON_FILE_PATH(JSON_FILE_PATH);
        bookService.setJSON_FILE_PATH("./src/test/java/testDB/books.json");
    }

    @Test
    public void standardPurchase() throws BookException, StockException {
        bookService.setJSON_FILE_PATH("./src/test/java/testDB/books.json");
        Book book = bookService.returnBook(5);
        List<String> genres = Arrays.asList("Fiction", "Dystopian", "Science Fiction");

        //ASSERT
        Assertions.assertEquals(book.getCountry(), "United Kingdom");
        Assertions.assertEquals(book.getYear(), 1932);
        Assertions.assertEquals(book.getAuthor(), "Aldous Huxley");
        Assertions.assertEquals(book.getGenre(), genres);
        Assertions.assertEquals(book.getPrice(), 67);
        Assertions.assertEquals(book.getName(), "Brave New World");
        Assertions.assertEquals(book.getActive(), true);
        Assertions.assertEquals(book.getPublisher(), "Penguin Random House");
        Assertions.assertEquals(book.getId(), 5);
        Assertions.assertEquals(book.getStock(), 9);

//        order = new Order(book, 7);
//        orderRecord = new OrderRecord("tvoje", "mama");
//        JSONObject jsonOrder = order.toJson();
        String order = "{\n" +
        "      \"name\": \"standardni nakup\",\n" +
        "      \"address\": \"421111 Main St.\",\n" +
        "      \"order\": [\n" +
        "        {\n" +
        "          \"amount\": 1,\n" +
        "          \"book_author\": \"Aldous Huxley\",\n" +
        "          \"id\": 5,\n" +
        "          \"book_name\": \"Brave New World\",\n" +
        "        }" +
        "      ]\n" +
        "    }";

        JSONObject jsonOrder = new JSONObject(order);
        purchaseService.purchaseBooks(jsonOrder);

        orderName = jsonOrder.getString("name");
        orderAddress = jsonOrder.getString("address");

        //assert
        Assertions.assertEquals("standardni nakup", orderName);
        Assertions.assertEquals("421111 Main St.", orderAddress);


        //KONTROLA PRIDANI DO DB
        //set up
        JSONObject json = common.loadJsonData(JSON_FILE_PATH);
        JSONArray recordsArray = json.getJSONArray("records");
        for (int i = 0; i < recordsArray.length(); i++){
            JSONObject orderObject = recordsArray.getJSONObject(i);
            address = orderObject.getString("address");
            name = orderObject.getString("name");
//            date = orderObject.getString("processed_on");
            JSONArray booksArray = orderObject.getJSONArray("order");
            for (int j = 0; j<booksArray.length(); j++){
                JSONObject bookObject = booksArray.getJSONObject(j);
                bookAmount = bookObject.getInt("amount");
                bookAuthor = bookObject.getString("book_author");
                bookId = bookObject.getInt("id");
                bookName = bookObject.getString("book_name");
            }

        }

        Assertions.assertEquals(address, orderAddress);
        Assertions.assertEquals(name, orderName);
        Assertions.assertEquals(1, bookAmount);
        Assertions.assertEquals("Aldous Huxley", bookAuthor);
        Assertions.assertEquals(5, bookId);
        Assertions.assertEquals("Brave New World", bookName);

        //VRACENI TESTDB DO PUVODNIHO STAVU - stock amount
        bookService.restockBook(5, 1);
        book.setStock(book.getStock() + 1);
    }

    @Test
    public void nullPurchase() throws BookException, StockException{
        JSONObject json = common.loadJsonData(JSON_FILE_PATH);
        JSONArray recordsArray = json.getJSONArray("records");

//    OBJEDNANI, SPRAVNA CHYBOVA HLASKA; zkontrolovat, ze do databaze nepribyla nova objednavka
        List<Book> book = bookService.getAllBooks("F. Scott Fitzgerald","Fiction");
        Book book1 = book.get(0);
        List<String> genres = Arrays.asList("Fiction", "Classic");

        //ASSERT
        Assertions.assertEquals(book1.getCountry(), "United States");
        Assertions.assertEquals(book1.getYear(), 1925);
        Assertions.assertEquals(book1.getAuthor(), "F. Scott Fitzgerald");
        Assertions.assertEquals(book1.getGenre(), genres);
        Assertions.assertEquals(book1.getPrice(), 114);
        Assertions.assertEquals(book1.getName(), "The Great Gatsby");
        Assertions.assertEquals(book1.getActive(), true);
        Assertions.assertEquals(book1.getPublisher(), "Wiley");
        Assertions.assertEquals(book1.getId(), 3);
        Assertions.assertEquals(book1.getStock(), 10);

        //KONTROLA DB PRED POKUSEM O KOUPENI KNIHY
        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountBefore++;
        }

        //SET UP
//        order = new Order(book1, 7);
//        orderRecord = new OrderRecord("tvoje", "mama");
//        JSONObject jsonOrder = order.toJson();
        String order = "{\n" +
                "      \"name\": \"null purchase\",\n" +
                "      \"address\": \"Dum 98\",\n" +
                "      \"order\": [\n" +
                "        {\n" +
                "          \"amount\": 0,\n" +
                "          \"book_author\": \"F. Scott Fitzgerald\",\n" +
                "          \"id\": 3,\n" +
                "          \"book_name\": \"The Great Gatsby\",\n" +
                "        }" +
                "      ]\n" +
                "    }";

        JSONObject jsonOrder = new JSONObject(order);
//        purchaseService.purchaseBooks(jsonOrder);
        orderName = jsonOrder.getString("name");
        orderAddress = jsonOrder.getString("address");

        //assert
        Assertions.assertEquals("null purchase", orderName);
        Assertions.assertEquals("Dum 98", orderAddress);

        Exception exception = Assertions.assertThrows(StockException.class, () -> {
                purchaseService.purchaseBooks(jsonOrder);
        });

        String expectedMessage = "Invalid amount.";

        Assertions.assertEquals(expectedMessage, exception.getMessage());


        //KONTROLA PRIDANI DO DB
        //set up

        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountAfter++;
        }

        Assertions.assertEquals(receiptAmountBefore, receiptAmountAfter);
    }

    @Test
    public void noAmountInput() throws BookException, StockException {//TODO from here
        JSONObject json = common.loadJsonData(JSON_FILE_PATH);
        JSONArray recordsArray = json.getJSONArray("records");

        bookService.setJSON_FILE_PATH("./src/test/java/testDB/books.json");
        Book book = bookService.returnBook(5);
        List<String> genres = Arrays.asList("Fiction", "Dystopian", "Science Fiction");

        //ASSERT
        Assertions.assertEquals(book.getCountry(), "United Kingdom");
        Assertions.assertEquals(book.getYear(), 1932);
        Assertions.assertEquals(book.getAuthor(), "Aldous Huxley");
        Assertions.assertEquals(book.getGenre(), genres);
        Assertions.assertEquals(book.getPrice(), 67);
        Assertions.assertEquals(book.getName(), "Brave New World");
        Assertions.assertEquals(book.getActive(), true);
        Assertions.assertEquals(book.getPublisher(), "Penguin Random House");
        Assertions.assertEquals(book.getId(), 5);
        Assertions.assertEquals(book.getStock(), 9);


        //KONTROLA DB PRED POKUSEM O KOUPENI KNIHY
        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountBefore++;
        }


//        order = new Order(book, 7);
//        orderRecord = new OrderRecord("tvoje", "mama");
//        JSONObject jsonOrder = order.toJson();
        String order = "{\n" +
                "      \"name\": \"noAmountInput\",\n" +
                "      \"address\": \"cukrKava.\",\n" +
                "      \"order\": [\n" +
                "        {\n" +
                "          \"book_author\": \"Aldous Huxley\",\n" +
                "          \"id\": 5,\n" +
                "          \"book_name\": \"Brave New World\",\n" +
                "        }" +
                "      ]\n" +
                "    }";

        JSONObject jsonOrder = new JSONObject(order);

        orderName = jsonOrder.getString("name");
        orderAddress = jsonOrder.getString("address");

        //assert
        Assertions.assertEquals("noAmountInput", orderName);
        Assertions.assertEquals("cukrKava.", orderAddress);

        Exception exception = Assertions.assertThrows(StockException.class, () -> {
            purchaseService.purchaseBooks(jsonOrder);
        });

        String expectedMessage = "Invalid amount.";

        Assertions.assertEquals(expectedMessage, exception.getMessage());

        //KONTROLA PRIDANI DO DB
        //set up

        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountAfter++;
        }

        Assertions.assertEquals(receiptAmountBefore, receiptAmountAfter);
    }
    @Test
    public void notEnoughStockPurchase() throws BookException, StockException {
        JSONObject json = common.loadJsonData(JSON_FILE_PATH);
        JSONArray recordsArray = json.getJSONArray("records");

//    vyhledani knihy podle zanru, objednani vice kusu, nez je in stock, spravna chybova hlaska
//    zkontrolovat, ze do databaze nepribyla nova objednavka

        //SET UP
        List<Book> book = bookService.getAllBooks("Jane Austen","Fiction");
        Book book1 = book.get(0);
        List<String> genres = Arrays.asList("Fiction", "Romance", "Classic");

        //ASSERT
        Assertions.assertEquals(book1.getCountry(), "United Kingdom");
        Assertions.assertEquals(book1.getYear(), 1813);
        Assertions.assertEquals(book1.getAuthor(), "Jane Austen");
        Assertions.assertEquals(book1.getGenre(), genres);
        Assertions.assertEquals(book1.getPrice(), 167);
        Assertions.assertEquals(book1.getName(), "Pride and Prejudice");
        Assertions.assertEquals(book1.getActive(), true);
        Assertions.assertEquals(book1.getPublisher(), "Wiley");
        Assertions.assertEquals(book1.getId(), 2);
        Assertions.assertEquals(book1.getStock(), 4);


        //KONTROLA DB PRED POKUSEM O KOUPENI KNIHY
        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountBefore++;
        }


        String order = "{\n" +
                "      \"name\": \"notEnoughStockPurchase\",\n" +
                "      \"address\": \"421111 Main St.\",\n" +
                "      \"order\": [\n" +
                "        {\n" +
                "          \"amount\": 5,\n" +
                "          \"book_author\": \"Jane Austen\",\n" +
                "          \"id\": 2,\n" +
                "          \"book_name\": \"Price and Prejudice\",\n" +
                "        }" +
                "      ]\n" +
                "    }";

        JSONObject jsonOrder = new JSONObject(order);

        orderName = jsonOrder.getString("name");
        orderAddress = jsonOrder.getString("address");

        //assert
        Assertions.assertEquals("notEnoughStockPurchase", orderName);
        Assertions.assertEquals("421111 Main St.", orderAddress);

        Exception exception = Assertions.assertThrows(StockException.class, () -> {
            purchaseService.purchaseBooks(jsonOrder);
        });

        String expectedMessage = "Pride and Prejudice (id = 2) only has 4 copies and you ordered 5";

        Assertions.assertEquals(expectedMessage, exception.getMessage());

        //KONTROLA DB PRED POKUSEM O KOUPENI KNIHY
        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountAfter++;
        }

        Assertions.assertEquals(receiptAmountAfter, receiptAmountBefore);
    }

    @Test
    public void deletedBookPurchase() throws BookException, StockException{
        JSONObject json = common.loadJsonData(JSON_FILE_PATH);
        JSONArray recordsArray = json.getJSONArray("records");

        //SET UP
//        List<Book> book = bookService.getAllBooks("George Orwell","Fiction");
//        Book book1 = book.get(0);
        Book book1 = bookService.returnBook(4);
        List<String> genres = Arrays.asList("Fiction", "Dystopian", "Science Fiction");

        //ASSERT
        Assertions.assertEquals(book1.getCountry(), "United Kingdom");
        Assertions.assertEquals(book1.getYear(), 1949);
        Assertions.assertEquals(book1.getAuthor(), "George Orwell");
        Assertions.assertEquals(book1.getGenre(), genres);
        Assertions.assertEquals(book1.getPrice(), 111);
        Assertions.assertEquals(book1.getName(), "1984");
        Assertions.assertEquals(book1.getActive(), false);
        Assertions.assertEquals(book1.getPublisher(), "RELX Group");
        Assertions.assertEquals(book1.getId(), 4);
        Assertions.assertEquals(book1.getStock(), 3);

        //KONTROLA DB PRED POKUSEM O KOUPENI KNIHY
        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountBefore++;
        }


        //SET UP
        String order = "{\n" +
                "      \"name\": \"deletedBookPurchase\",\n" +
                "      \"address\": \"421111 Main St.\",\n" +
                "      \"order\": [\n" +
                "        {\n" +
                "          \"amount\": 1,\n" +
                "          \"book_author\": \"George Orwell\",\n" +
                "          \"id\": 4,\n" +
                "          \"book_name\": \"1984\",\n" +
                "        }" +
                "      ]\n" +
                "    }";

        JSONObject jsonOrder = new JSONObject(order);
//        purchaseService.purchaseBooks(jsonOrder);
        orderName = jsonOrder.getString("name");
        orderAddress = jsonOrder.getString("address");

        //assert
        Assertions.assertEquals("deletedBookPurchase", orderName);
        Assertions.assertEquals("421111 Main St.", orderAddress);

        Exception exception = Assertions.assertThrows(BookException.class, () -> {
            purchaseService.purchaseBooks(jsonOrder);
        });

        String expectedMessage = "The book with ID: 4 doesn't exist!";

        Assertions.assertEquals(expectedMessage, exception.getMessage());

        //KONTROLA DB PRED POKUSEM O KOUPENI KNIHY
        for (int i = 0; i < recordsArray.length(); i++){
            receiptAmountAfter++;
        }

        Assertions.assertEquals(receiptAmountAfter, receiptAmountBefore);
    }

}